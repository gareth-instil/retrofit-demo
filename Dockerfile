FROM openjdk:jre-alpine
WORKDIR /opt/app/
COPY build/libs/translations-0.0.1.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
