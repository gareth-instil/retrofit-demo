package com.instil.greetings.translation;

import com.google.common.base.Charsets;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class TranslationCache {

    private Map<String, TranslationResult> cache = new HashMap<>();

    public String generateCacheKeyFor(String... values) {
        Hasher hasher = Hashing.sha256().newHasher();
        for (String value : values) {
            hasher.putString(String.format(value), Charsets.UTF_8);
        }
        return hasher.hash().toString();
    }

    public Optional<TranslationResult> get(String key) {

        return Optional.ofNullable(cache.get(key));
    }

    public void put(String key, TranslationResult value) {
        cache.put(key, value);
    }

}
