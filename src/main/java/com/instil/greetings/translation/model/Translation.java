package com.instil.greetings.translation.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Translation {
	
	@JsonProperty private int code;
	@JsonProperty private String lang;
	@JsonProperty private List<String> text;

	public String first() {
		return text.get(0);
	}
	
}
