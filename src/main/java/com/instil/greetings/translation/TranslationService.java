package com.instil.greetings.translation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Optional;

@Service
public class TranslationService {

    @Autowired
    private TranslationCache translationCache;

    @Autowired
    private TranslationApiConfiguration translationApiConfiguration;

    @Autowired
    private TranslationAPIService translationAPIService;

    public TranslationResult translationFor(String text, Optional<String> lang) {
            return translationFor(text, lang.orElse(translationApiConfiguration.getDefaultLang()));
    }

    public TranslationResult translationFor(String text, String lang) {
        if (lang.equals(translationApiConfiguration.getDefaultLang())) {
            return buildTranslationResultFor(text, text, lang);
        }
        return cacheEntryFor(text, lang)
                .orElseGet(
                        () -> {
                            String translated = translationAPIService.translate(text, lang);
                            TranslationResult result = buildTranslationResultFor(text, translated, lang);
                            addCacheEntryFor(result);
                            return result;
                        }
                );
    }

    private TranslationResult buildTranslationResultFor(String original, String translated, String lang) {
        return TranslationResult.builder()
                .originalText(original)
                .translatedText(translated)
                .lang(lang)
                .build();
    }

    private Optional<TranslationResult> cacheEntryFor(String text, String lang) {
        String key = translationCache.generateCacheKeyFor(text, lang);
        return translationCache.get(key);
    }

    private void addCacheEntryFor(TranslationResult result) {
        String key = translationCache.generateCacheKeyFor(result.getOriginalText(), result.getLang());
        translationCache.put(key, result);
    }
}
