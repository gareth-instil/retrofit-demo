package com.instil.greetings.translation;

import java.util.Map;

import com.instil.greetings.translation.model.Translation;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface TranslationAPI {

	@GET("translate")
	Call<Translation> translate(@Query("text") String text, @Query("lang") String lang);
	
}
