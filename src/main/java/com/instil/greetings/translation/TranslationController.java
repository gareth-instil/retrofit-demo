package com.instil.greetings.translation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class TranslationController {

	@Autowired
    private TranslationService translationService;
	
	@RequestMapping(path="/translate", produces="application/json")
	public TranslationResult translate(
			@RequestBody Optional<String> text,
			@RequestParam(value="lang", required=false) Optional<String> lang) {
		return translationService.translationFor(text.orElse(""), lang);
	}
	
}
