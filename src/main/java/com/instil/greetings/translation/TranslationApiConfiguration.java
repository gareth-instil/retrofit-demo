package com.instil.greetings.translation;

import lombok.Data;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Data
@Component
@ConfigurationProperties("api")
public class TranslationApiConfiguration {

    private String url;
    private String key;
    private String defaultLang;

    @Bean
    private TranslationAPI translationAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(okHttpClient())
                .build();
        return retrofit.create(TranslationAPI.class);
    }

    private OkHttpClient okHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor((chain) -> {
            Request originalRequest = chain.request();
            return chain.proceed(updateQueryParameters(originalRequest));
        });
        return httpClient.build();
    }

    private Request updateQueryParameters(Request originalRequest) {
        HttpUrl originalUrl = originalRequest.url();
        String lang = updateLangParameter(originalUrl);
        HttpUrl url = addUpdatedQueryParameters(originalUrl, lang);
        Request.Builder requestBuilder = originalRequest.newBuilder().url(url);
        return requestBuilder.build();
    }

    private HttpUrl addUpdatedQueryParameters(HttpUrl originalUrl, String lang) {
        return originalUrl.newBuilder()
                            .addQueryParameter("key", key)
                            .setQueryParameter("lang", lang)
                            .build();
    }

    private String updateLangParameter(HttpUrl originalUrl) {
        String lang = originalUrl.queryParameter("lang");
        return String.format("%s-%s", defaultLang, lang);
    }

}
