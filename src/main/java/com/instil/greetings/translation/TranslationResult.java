package com.instil.greetings.translation;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TranslationResult {

    private String originalText;
    private String translatedText;
    private String lang;

}
