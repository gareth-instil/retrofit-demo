package com.instil.greetings.translation;

import com.google.common.collect.ImmutableMap;
import com.instil.greetings.translation.model.Translation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.Map;

@Service
public class TranslationAPIService {

    @Autowired
    private TranslationAPI translationAPI;

    public String translate(String text, String lang) {
        try {
            Response<Translation> translation = translationAPI.translate(text, lang).execute();
            return getTranslatedTextFrom(translation.body());
        } catch (IOException ioe) {
            return text;
        }
    }

    private String getTranslatedTextFrom(Translation translation) {
        return translation.getText().get(0);
    }


}
