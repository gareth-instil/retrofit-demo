package com.instil.greetings.translation;

import com.instil.greetings.translation.model.Translation;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class TranslationCacheTest {

    private TranslationCache target = new TranslationCache();

    @Test
    public void shouldReturnAnEmptyOptionalForANonExistingEntry() {
        assertThat(target.get("key").isPresent()).isFalse();
    }

    @Test
    public void shouldReturnAnOptionalWithValueForExistingEntry() {
        TranslationResult result = translationResult();
        target.put("key", result);

        Optional<TranslationResult> value = target.get("key");
        assertThat(value.isPresent()).isTrue();
        assertThat(value.get()).isEqualTo(result);
    }

    private TranslationResult translationResult() {
        return TranslationResult.builder()
                .originalText("flib")
                .translatedText("flob")
                .lang("flibber")
                .build();
    }

}