package com.instil.greetings.translation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TranslationServiceTests {

    @Mock
    private TranslationAPIService translationAPIService;

    @Mock
    private TranslationCache translationCache;

    @Mock
    private TranslationApiConfiguration translationApiConfiguration;

    @InjectMocks
    private TranslationService target;

    @Test
    public void shouldNotFetchFromApiIfValueIsCached() {
        String text = "text";
        String lang = "lang";

        TranslationResult result = buildTranslationResultFor(text, lang);
        when(translationCache.generateCacheKeyFor(anyVararg())).thenReturn(text);
        when(translationCache.get(eq(text))).thenReturn(Optional.of(result));

        target.translationFor(text, lang);

        verify(translationAPIService, never()).translate(text, lang);
    }

    private TranslationResult buildTranslationResultFor(String text, String lang) {
        return TranslationResult.builder()
                    .originalText(text)
                    .translatedText(text)
                    .lang(lang)
                    .build();
    }

    @Test
    public void shouldFetchFromApiIfValueIsNotCached() {
        String text = "text";
        String lang = "lang";

        when(translationCache.generateCacheKeyFor(anyVararg())).thenReturn(text);
        when(translationCache.get(eq(text))).thenReturn(Optional.empty());

        target.translationFor(text, lang);

        verify(translationAPIService, times(1)).translate(text, lang);
    }

    @Test
    public void shouldReturnArgumentWhenUsingDefaultLanguage() {
        String text = "text";
        String defaultLang = "klingon";

        when(translationApiConfiguration.getDefaultLang()).thenReturn(defaultLang);

        TranslationResult result = target.translationFor(text, defaultLang);

        assertThat(result.getTranslatedText()).isEqualTo(text);
    }

}